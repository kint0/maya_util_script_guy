"""
	Maya File Export Utility 
	created by pp.
"""
import os
import sys
import stat
import maya.cmds as mc
import maya.mel as mel
import re
import pprint

PATH = os.path.split(__file__)[0].replace("\\","/") + "/"

class PublishFileUtil(object):
	def __init__(self,objName):
		super(PublishFileUtil,self).__init__()
		self.objName = objName
		self.fileCurrentPathAndName = mc.file(q = True,exn = True) 
		if re.findall(r"\w+_\w+_\w+_\w+_v[0-9]{3}.\w+",self.fileCurrentPathAndName):
			self.fileName = re.findall(r"\w+_\w+_\w+_\w+_v[0-9]{3}.\w+",self.fileCurrentPathAndName)[0]
			self.currentFilePath = re.sub(self.fileName,"",self.fileCurrentPathAndName) #set this through combine name func
			fileNameList = self.fileName.split("_") #leave this alone
			self.fileName_type = fileNameList[0]
			self.fileName_name = fileNameList[1]
			self.fileName_step = fileNameList[2]
			self.fileName_task = fileNameList[3]
			self.fileName_version = int(re.findall(r"\d+",self.fileName)[0])
		else:
			raise ImportError("Current File Is Not Saved")
	def __str__(self):
		return self.objName

	def __repr__(self):
		return self.objName

	@staticmethod
	def classInfo():
		print "PublishFileUtil class : use for store filepath and name of \ncurrently saved maya scene which is opening"
		method_list = dir(PublishFileUtil)[18:]
		print "Method : \n"
		pprint.pprint(method_list)

	@property
	def _objName(self):
		return self.objName
	
	@_objName.setter
	def _objName(self,val):
		self.objName = val
	
	@property
	def _fileName_type(self):
		return self.fileName_type
	
	@_fileName_type.setter
	def _fileName_type(self,val):
		self.fileName_type = val

	@property
	def _fileName_name(self):
		return self.fileName_name
	
	@_fileName_name.setter
	def _fileName_name(self,val):
		self.fileName_name = val

	@property
	def _fileName_step(self):
		return self.fileName_step
	
	@_fileName_step.setter
	def _fileName_step(self,val):
		self.fileName_step = val

	@property
	def _fileName_task(self):
		return self.fileName_task
	
	@_fileName_task.setter
	def _fileName_task(self,val):
		self.fileName_task = val

	@property
	def _fileName_version(self):
		return self.fileName_version
	
	@_fileName_version.setter
	def _fileName_version(self,val):
		self.fileName_version = val

	@property
	def _fileCurrentPathAndName(self):
		return self.fileCurrentPathAndName
	
	@_fileCurrentPathAndName.setter
	def _fileCurrentPathAndName(self,val):
		self.fileCurrentPathAndName = val

	@property
	def _fileName(self):
		print "101"
		self.setFileNameInFormat()
		return self.fileName

	@_fileName.setter
	def _fileName(self,val):
		self.fileName = val
	
	def setFileNameInFormat(self):
		self.fileName = "%s_%s_%s_%s_v%03d" % (self.fileName_type,self.fileName_name,
			self.fileName_step,self.fileName_task,self.fileName_version)

	


def checkSameFile(filePath,newName = ""):
		#input path and full new name(aa_aa_aa_aa_001)
		filesInDir = []
		for(dirpath, dirnames, filenames) in os.walk(filePath):
			for f in filenames:
				filesInDir.append(f)
			break
		filesInForms = []

		for f in filesInDir:
			if re.search(r"(\w+)_(\w+)_(\w+)_(\w+)_v([0-9]{3})",f):
				filesInForms.append(f)
		
		del filesInDir

		for f in filesInForms:
			if re.findall(r"(\D+)",f)[0] == re.findall(r"(\D+)",newName)[0]:
				ver = re.findall(r"_v([0-9]{3})",f)[0]
				newVer = "%03d" % (int(ver)+1) # adding to a new version
				newName = re.findall(r"(\D+)",newName)[0] + newVer
				#CALL CREATE ON THIS
		return newName



# separate to class
def editNewName(oldName,filePath,fileType):
	if fileType == "OBJ":
		nextStep = "texture"
	elif fileType == "CPU":
		nextStep = "rig"
	elif fileType == "GPU":
		nextStep = "dress"

	nTuple = re.findall(r"(\w+)_(\w+)_(\w+)_(\w+)_v([0-9]{3})",oldName)[0]
	newName = nTuple[0]+"_"+nTuple[1]+"_"+nextStep+"_"+nTuple[3]+fileType+"_v"+nTuple[4]
	newName = checkSameFile(filePath,newName)
	return newName

class ExportInMA(PublishFileUtil):
	def __init__(self, objName):
		super(ExportInMA, self).__init__(objName)

	@staticmethod
	def classInfo():
		print "ExportInMA class : use for export object in current opened file in maya in .ma extension"
		method_list = dir(ExportInMA)[18:]
		print "Method : \n"
		pprint.pprint(method_list)

	def exportMAActivate(self):
		fileName = checkSameFile(self.currentFilePath,self._fileName)
		mc.select(self.objName)
		mc.file(self.currentFilePath +"/"+ fileName , force=True, type='mayaAscii', pr=True, es=True)
		mc.select(clear = True)
		self.fileCurrentPathAndName = self.currentFilePath +"/"+ fileName + ".ma"
		readOnlyFile(self.fileCurrentPathAndName)

	#  to set file be read only os.chmod(filePath +"/"+ fileName + ".ma", stat.S_IRUSR|stat.S_IRGRP|stat.S_IROTH)


class ExportInObj(PublishFileUtil):
	def __init__(self,objName,grpTog = True, ptGrpTog = True, matTog = True, smoothTog = True, normsTog = True):
		super(ExportInObj, self).__init__(objName)
		self.grpTog = grpTog
		self.ptGrpTog = ptGrpTog
		self.matTog = matTog
		self.smoothTog = smoothTog
		self.normsTog = normsTog


	@staticmethod
	def classInfo():
		print "ExportInObj class : use for export object in current opened file in maya in .obj extension\nto work in texture step"
		method_list = dir(ExportInObj)[18:]
		print "Method : \n"
		pprint.pprint(method_list)

	def exportObjActivate(self):
		grpTog = str(int(self.grpTog))
		ptGrpTog = str(int(self.ptGrpTog))
		matTog = str(int(self.matTog))
		smoothTog = str(int(self.smoothTog))
		normsTog = str(int(self.normsTog))

		self.fileName_task = self.fileName_task + "OBJ"
		self.fileName = checkSameFile(self.currentFilePath,self._fileName)

		optionsReflag = 'groups={0};ptgroups={1};materials={2};smoothing={3};normals={4}'.format(grpTog,ptGrpTog,matTog,smoothTog,normsTog)

		mc.select(self.objName)
		mc.file(self.currentFilePath +"/"+ self.fileName , force=True, options=optionsReflag, type='OBJexport', pr=True, es=True)
		mc.select(clear = True)
		self.fileCurrentPathAndName = self.currentFilePath +"/"+ self.fileName +".obj"
		readOnlyFile(self.fileCurrentPathAndName)

class ExportInAbcCpu(PublishFileUtil):
	def __init__(self,objName,frameStart = 1,frameEnd = 1,ro = False,writeVis = False,worldSp = False):
		super(ExportInAbcCpu, self).__init__(objName)
		self.frameStart = frameStart
		self.frameEnd = frameEnd
		self.ro = ro
		self.writeVis = writeVis
		self.worldSp = worldSp

	@staticmethod
	def classInfo():
		print "ExportInAbcCpu class : use for export object in current opened file in maya in .abc(CPU) extension\nto work in rigging step"
		method_list = dir(ExportInAbcCpu)[18:]
		print "Method : \n"
		pprint.pprint(method_list)


	def exportCpuActivate(self):
		if self.ro:
			ro = "-ro "
		else:
			ro = ""
		
		if self.writeVis:
			writeVis = "-writeVisibility "
		else:
			writeVis = ""
		
		if self.worldSp:
			worldSp = "-worldSpace "
		else:
			worldSp = ""

		self.fileName_task = self.fileName_task + "CPU"
		self.fileName = checkSameFile(self.currentFilePath,self._fileName)

		mel_cmd = 'AbcExport -j "-frameRange {0} {1} {2}{3}{4}-dataFormat ogawa -root |{5} -file {6}{7}.abc"'.format(self.frameStart,
			self.frameEnd,ro,writeVis,worldSp,self.objName,self.currentFilePath,self.fileName)
		# print mel_cmd
		mel.eval(mel_cmd)
		self.fileCurrentPathAndName = self.currentFilePath +"/"+ self.fileName +".abc"
		readOnlyFile(self.fileCurrentPathAndName)
	

class ExportInAbcGpu(PublishFileUtil):
	def __init__(self,objName,frameStart = 1,frameEnd = 1,writeMatTog = True):
		super(ExportInAbcGpu, self).__init__(objName)
		self.frameStart = frameStart
		self.frameEnd = frameEnd
		self.writeMatTog = writeMatTog

	@staticmethod
	def classInfo():
		print "ExportInAbcCpu class : use for export object in current opened file in maya in .abc(GPU) extension\nto work in setdress step"
		method_list = dir(ExportInAbcGpu)[18:]
		print "Method : \n"
		pprint.pprint(method_list)

	def exportGpuActivate(self):	
		if self.writeMatTog:
			writeMatTog = "-writeMaterials "
		else:
			writeMatTog = ""

		self.fileName_task = self.fileName_task + "GPU"
		self.fileName = checkSameFile(self.currentFilePath,self._fileName)

		mel_cmd = 'gpuCache -startTime {0} -endTime {1} -optimize -optimizationThreshold 40000 {2}-dataFormat ogawa -directory "{3}" -fileName "{4}" {5};'.format(self.frameStart,
			self.frameEnd,writeMatTog,self.currentFilePath,self.fileName,self.objName)
		mel.eval(mel_cmd)
		self.fileCurrentPathAndName = self.currentFilePath +"/"+ self.fileName +".abc"
		readOnlyFile(self.fileCurrentPathAndName)

def writeableFile(filePathWithName):
	os.chmod(filePathWithName, stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)	


def readOnlyFile(filePathWithName):
	os.chmod(filePathWithName, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)

def main():
	
	# using step is call XXnameCreator-->call checkSameFile-->export
	
	# a = PublishFileUtil("hairDef")
	# a.fileName_type = "PPTX"
	
	# b = ExportInMA("hairDef")
	ExportInMA.classInfo()
	ExportInObj.classInfo()
	ExportInAbcGpu.classInfo()
	ExportInAbcCpu.classInfo()
	
	# print b._fileName_version
	# print b._fileName
	# b.exportMAActivate()



	# print a.fileName
	
	# editableImportFile("GGEZ")
	# a.exportInObj("hairDef")
	# a.exportInMA("hairDef")
	# b.exportInAbcCpu("pSphere1",newName,"E:/ygg/exportor/",1,1,1,1,1)
	# b.exportInAbcGpu("pSphere1",newName,"E:/ygg/exportor/",1,1,1)
	# b.exportInObj("pSphere1","E:/ygg/exportor/%s.obj" % newName,grpTog = 0,ptGrpTog = 0,matTog = 0)
	# newfilename = PATH + "char_nimbee_model_ABC_001"
	# print newfilename
	# exportInObj( "Quovo", newfilename, grpTog = 0, ptGrpTog = 0, matTog = 0 )
	# nameCreator(oldName = "type_name_step_task_001")
	
	
if __name__ == "__main__" :
	main()
	