import maya.cmds as mc
import os
import sys
import re


class parentObj(object):
	def __init__(self,name):
		super(parentObj,self).__init__()
		self.name = name
	def __str__(self):
		return self.name

	def __repr__(self):
		return self.name

	def __add__(self,children):
		mc.polyUnite(self.name,children.name,n = self.name)
		mc.delete(all = True,ch = True)

	def __rshift__(self,children):
		mc.parent(self.name,children.name)
		mc.delete(all = True,ch = True)

def cmdAndCoutObj(sels,comm):
	if len(sels) == 2:
		objA = parentObj(sels[0])
		objB = parentObj(sels[1])
		if comm == "parent":
			objA >> objB
		elif comm == "combine":
			objA + objB
		return True

	else:
		return False

def main():
	sels = mc.ls(sl = True)
	cmdAndCoutObj(sels,"combine")
	
	

if __name__ == "__main__":
	main()