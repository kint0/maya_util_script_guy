import os
import sys
import maya.cmds as mc
import re
import maya.mel as mel

class DAG(object):
	def __init__(self, name = ""):
		super(DAG,self).__init__()
		self.name = name

	def __str__(self):
		return self.name

	def __repr__(self):
		return self.name

	@property
	def _name(self):
		return self.name 

	@_name.setter
	def _name(self,val):
		self.name = val

class Transform(DAG):
	def __init__(self, position = [0,0,0],rotate = [0,0,0],scale = [1,1,1]):
		super(Transform,self).__init__()
		self.position = position
		self.scale = scale
		self.rotate = rotate

	def __str__(self):
		return self.position,self.rotate,self.scale

	def __repr__(self):
		return self.position,self.rotate,self.scale

	@property
	def _position(self):
		return self._position

	@_position.setter
	def _position(self,val):
		self.position = val

	@property
	def _rotate(self):
		return self._rotate

	@_rotate.setter
	def _rotate(self,val):
		self.rotate = val
	
	@property
	def _scale(self):
		return mc.xform(self.name,q = True,t = True)

	@_scale.setter
	def _scale(self,val):
		self.scale = val

		
class Control(Transform):
	def __init__(self, shape = "",color = ""):
		super(Control,self).__init__()
		self.shape = shape
		self.color = color 
		if mc.objExists(self.name):
			self.shapeName = mc.listRelatives(self.name, s=True)[0]

		self.colorDict = {"black" : 1,"darkGrey" : 2,"lightGrey" : 3,"darkRed" : 4,
						  "darkBlue" : 5, "lightBlue" : 6,"darkGreen" : 7,"darkPurple":8,
						  "pink":9,"lightBrown":10,"darkBrown":11,"darkRedTwo":12,
						  "lightRed":13,"lightGreen":14,"darkBlue":15,"white":16,
						  "yellow":17,"cyan":18,"veridian":19,"rose":20,
						  "lightBrownTwo":21,"lightYellow":22,"green":23,
						  "lightBrownThree":24,"yellowBrown":25,"greenBrown":26,
						  "lightGreenTwo":27,"darkCyan":28,"darkBlueTwo":29,
						  "purple":30,"redPink":31
						  }

	def __str__(self):
		return  self.shape

	def __repr__(self):
		return self.shape	

	@property
	def _shapeName(self):
		return self._shapeName

	@_shapeName.setter
	def _shapeName(self,val):
		self._shapeName = val

	@property
	def _shape(self):
		return self._shape

	@_shape.setter
	def _shape(self,val):
		self._shape = val

	@property
	def _color(self):
		return mc.getAttr("%s.overrideColor" % self.shapeName) 

	@_color.setter
	def _color(self,val):
		mc.setAttr('%s.overrideEnabled' % self.shapeName, 1)
		mc.setAttr('%s.overrideColor' % self.shapeName, self.colorDict[val])
		print self.shapeName

	def setTransformOfObj(self,position = [0,0,0]):
		mc.xform(self.name,t = position)

	def checkToCreateShape(self):

		if self.shape == "circle":
			mel.eval("curve -d 1 -p 2.23517e-07 0 -1.000001 -p 0.156435 0 -0.987689 -p 0.309017 0 -0.951057 -p 0.453991 0 -0.891007 -p 0.587786 0 -0.809017 -p 0.707107 0 -0.707107 -p 0.809018 0 -0.587785 -p 0.891007 0 -0.453991 -p 0.951057 0 -0.309017 -p 0.987689 0 -0.156434 -p 1 0 0 -p 0.987688 0 0.156434 -p 0.951057 0 0.309017 -p 0.891007 0 0.453991 -p 0.809017 0 0.587785 -p 0.707107 0 0.707107 -p 0.587785 0 0.809017 -p 0.453991 0 0.891007 -p 0.309017 0 0.951057 -p 0.156434 0 0.987689 -p -1.04308e-07 0 1 -p -0.156435 0 0.987689 -p -0.309017 0 0.951057 -p -0.453991 0 0.891007 -p -0.587786 0 0.809017 -p -0.707107 0 0.707107 -p -0.809017 0 0.587785 -p -0.891007 0 0.45399 -p -0.951057 0 0.309017 -p -0.987689 0 0.156434 -p -1 0 -1.63913e-07 -p -0.987689 0 -0.156435 -p -0.951057 0 -0.309017 -p -0.891007 0 -0.453991 -p -0.809017 0 -0.587786 -p -0.707107 0 -0.707107 -p -0.587785 0 -0.809018 -p -0.453991 0 -0.891007 -p -0.309017 0 -0.951057 -p -0.156434 0 -0.987689 -p 2.23517e-07 0 -1.000001 -k 0 -k 1 -k 2 -k 3 -k 4 -k 5 -k 6 -k 7 -k 8 -k 9 -k 10 -k 11 -k 12 -k 13 -k 14 -k 15 -k 16 -k 17 -k 18 -k 19 -k 20 -k 21 -k 22 -k 23 -k 24 -k 25 -k 26 -k 27 -k 28 -k 29 -k 30 -k 31 -k 32 -k 33 -k 34 -k 35 -k 36 -k 37 -k 38 -k 39 -k 40 ;")
			sel = mc.ls(sl = True)
			mc.rename(sel[0],self.name)
			self.shapeName = mc.listRelatives(self.name, s=True)[0]
			 
		elif self.shape == "square":
			mel.eval("curve -d 1 -p -1 0 -1 -p -1 0 1 -p 1 0 1 -p 1 0 -1 -p -1 0 -1 -k 0 -k 1 -k 2 -k 3 -k 4 ;")
			sel = mc.ls(sl = True)
			mc.rename(sel[0],self.name)
			self.shapeName = mc.listRelatives(self.name, s=True)[0]

		elif self.shape == "cube":
			mel.eval("curve -d 1 -p -1 1 1 -p 1 1 1 -p 1 1 -1 -p -1 1 -1 -p -1 1 1 -p -1 -1 1 -p 1 -1 1 -p 1 1 1 -p 1 1 -1 -p 1 -1 -1 -p -1 -1 -1 -p -1 1 -1 -p -1 1 1 -p -1 -1 1 -p -1 -1 -1 -p 1 -1 -1 -p 1 -1 1 -k 0 -k 1 -k 2 -k 3 -k 4 -k 5 -k 6 -k 7 -k 8 -k 9 -k 10 -k 11 -k 12 -k 13 -k 14 -k 15 -k 16 ;")
			sel = mc.ls(sl = True)
			mc.rename(sel[0],self.name)
			self.shapeName = mc.listRelatives(self.name, s=True)[0]
			
		else:
			raise IOError("!!! invalid curve shape name input !!!")

def namingCtrlNorms(organ = "",side = "",typeN = "CTRL"):
	named = "%s_%s_%s" % (organ,side,typeN)
	return named

def main():
	crv = Control("square")
	crv.name = namingCtrlNorms("Foot","R")
	crv.checkToCreateShape()
	crv._color = "redPink"

if __name__ == "__main__":
	main()
