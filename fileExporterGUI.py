import os
import sys
import maya.cmds as mc
import maya.OpenMayaUI as omui

from PySide2.QtWidgets import * 
from PySide2.QtGui import *
from PySide2.QtCore import *

from shiboken2 import wrapInstance

PTR = wrapInstance(long(omui.MQtUtil.mainWindow()),QWidget)
PATH = os.path.split(__file__)[0].replace('\\','/') + "/"

class FileExporterGUI(QDialog):
	def __init__(self, parent = None):
		super(FileExporterGUI,self).__init__(parent)
		self.resize(500,500)
		self.setWindowTitle("File Exporter")

		self.mainV_layout = QVBoxLayout()
		self.setLayout(self.mainV_layout)

		self.path_widget = QWidget()
		self.path_layout = QHBoxLayout()
		self.path_widget.setLayout(self.path_layout)

		self.path_label = QLabel("Path :",self)
		self.path_lineEdit = QLineEdit()
		self.path_button = QPushButton("Browse",self)
		self.path_button.clicked.connect(self.getFileDirectoryPath)

		self.path_layout.addWidget(self.path_label)
		self.path_layout.addWidget(self.path_lineEdit)
		self.path_layout.addWidget(self.path_button)

		self.info_widget = QWidget()
		self.info_layout = QHBoxLayout()
		self.info_widget.setLayout(self.info_layout)

		self.name_label = QLabel("Name :",self)
		self.getName_label = QLabel(" - ",self)

		if mc.file( q=True, sn=True) != "":
			fileName = re.findall(r"\w+_\w+_\w+_\w+_v[0-9]{3}.\w+",mc.file( q=True, sn=True))[0]
			self.getName_label.setText(fileName)

		self.info_layout.addWidget(self.name_label)
		self.info_layout.addWidget(self.getName_label)


		self.mainTab_widget = QTabWidget()
		# self.mainTab_widget.setFixedSize(100,100)
		#obj tab
		self.objV_widget = QWidget()
		self.objV_layout = QVBoxLayout()
		self.objV_widget.setLayout(self.objV_layout)

		#group Radio
		self.objGroupH_widget = QWidget()
		self.objGroupH_widget.setFixedSize(200,50)
		self.objGroupH_layout = QHBoxLayout()
		self.objGroupH_widget.setLayout(self.objGroupH_layout)
		self.objGroupH_label = QLabel("Group : ",self)

		self.objGroupH_radioGroup = QGroupBox()
		self.objGroupHradio_layout = QHBoxLayout()
		self.objGroupH_on_radioButton = QRadioButton("on",self)
		self.objGroupH_off_radioButton = QRadioButton("off",self)
		self.objGroupHradio_layout.addWidget(self.objGroupH_on_radioButton)
		self.objGroupHradio_layout.addWidget(self.objGroupH_off_radioButton)
		self.objGroupHradio_layout.addStretch(1)
		self.objGroupH_radioGroup.setLayout(self.objGroupHradio_layout)

		self.objGroupH_layout.addWidget(self.objGroupH_label)
		self.objGroupH_layout.addWidget(self.objGroupH_radioGroup)

		#ptgroup Radio
		self.objPtGroupH_widget = QWidget()
		self.objPtGroupH_widget.setFixedSize(200,50)
		self.objPtGroupH_layout = QHBoxLayout()
		self.objPtGroupH_widget.setLayout(self.objPtGroupH_layout)
		self.objPtGroupH_label = QLabel("Points Group : ",self)

		self.objPtGroupH_radioGroup = QGroupBox()
		self.objPtGroupHradio_layout = QHBoxLayout()
		self.objPtGroupH_on_radioButton = QRadioButton("on",self)
		self.objPtGroupH_off_radioButton = QRadioButton("off",self)
		self.objPtGroupHradio_layout.addWidget(self.objPtGroupH_on_radioButton)
		self.objPtGroupHradio_layout.addWidget(self.objPtGroupH_off_radioButton)
		self.objPtGroupHradio_layout.addStretch(1)
		self.objPtGroupH_radioGroup.setLayout(self.objPtGroupHradio_layout)

		self.objPtGroupH_layout.addWidget(self.objPtGroupH_label)
		self.objPtGroupH_layout.addWidget(self.objPtGroupH_radioGroup)
		

		self.objV_layout.addWidget(self.objGroupH_widget)
		self.objV_layout.addWidget(self.objPtGroupH_widget)

		self.mainTab_widget.addTab(self.objV_widget,"Texture")

		#abcC tab
		self.abcCpuV_widget = QWidget()
		self.abcCpuV_layout = QVBoxLayout()
		self.abcCpuV_widget.setLayout(self.abcCpuV_layout)

		self.mainTab_widget.addTab(self.abcCpuV_widget,"Rigging")

		#abcG tab
		self.abcGpuV_widget = QWidget()
		self.abcGpuV_layout = QVBoxLayout()
		self.abcGpuV_widget.setLayout(self.abcGpuV_layout)

		self.mainTab_widget.addTab(self.abcGpuV_widget,"Dressing")

		self.mainV_layout.addWidget(self.path_widget)
		self.mainV_layout.addWidget(self.info_widget)
		self.mainV_layout.addWidget(self.mainTab_widget)

	def getFileDirectoryPath(self):
		directory = QFileDialog.getExistingDirectory()
		self.path_lineEdit.setText(directory)

def main():
	global ui
	try:
		ui.close()
	except:
		pass
	ui = FileExporterGUI(parent = PTR)
	ui.show()

if __name__ == '__main__':
	main()