import re

class strObj(object):
	def __init__(self,name):
		super(strObj,self).__init__()
		self.name = name

	def __str__(self):
		return self.name

	def __repr__(self):
		return self.name

	def capitalize(self):
		return self.name.capitalize()

	def casefold(self):
		return self.name.casefold()

	def center(self,leng,char):
		if type(leng) is int and type(char) is char: 
			return self.name.center(leng,char)
		else:
			raise TypeError("Argument Type is wrong (int,str)")

	def count(self,subs):
		counter = 0
		if type(subs) is str:
			for i in range(len(self.name)):
				if subs[0] == self.name[i]:
					if self.name[i:i+len(subs)] == subs:
						i+=len(subs)
						counter+=1
		else:
			raise TypeError("input type is not string".format(subs))
		return counter

	def endswith(self,ender):
		return self.name.endswith(str(ender))

	def upper(self):
		return self.name.upper()

	def lower(self):
		return self.name.lower()

	def title(self):
		return self.name.title()

	def split(self,subs):
		if type(subs) is str:
			return self.name.split(subs)
		else:
			return TypeError("{} type is not string".format(subs))
	
	def sort(self):
		sort_lst = re.findall(".",self.name)
		for i in range(len(sort_lst)-1):
			swapped = False
			for j in range(len(sort_lst)-i-1):
				if sort_lst[j] > sort_lst[j+1]:
					sort_lst[j],sort_lst[j+1] = sort_lst[j+1],sort_lst[j]
					swapped = True
			if swapped == False:
				break
		sort_str = ""
		for k in sort_lst:
			sort_str += k
		return sort_str

	def __add__(self,subs):
		return self.name + str(subs)

	def __sub__(self,subs):
		if str(subs) in self.name:
			return self.name.replace(str(subs),"")
		else:
			raise KeyError("{} not in {}".format(str(subs),self.name))

	def __getitem__(self,num):
		if type(num) is int:
			if len(self.name) > num and num >= -len(self.name) :
				return self.name[num]
			else:
				raise IndexError("index out of range")
		else:
			raise TypeError("{}'s' type is not int".format(num))

	def __mul__(self,num):
		if type(num) is int:
			return self.name * num
		else:
			raise TypeError("wrong input type (input isn't int)")

	def __lshift__(self,subs):
		subs = str(subs)
		newName = ""
		for i in range(len(self.name)):
			newName += self.name[i] + subs
		return newName

	def __rshift__(self,subs):
		subs = str(subs)
		newName = ""
		for i in range(len(subs)):
			newName += subs[i] + self.name 
		return newName

	def __invert__(self):
		newName = ""
		for i in range(len(self.name),0,-1):
			newName += self.name[i-1]
		return newName

	def __lt__(self,subs):
		return self.name < str(subs)

	def __le__(self,subs):
		return self.name <= str(subs)

	def __eq__(self,subs):
		return self.name == str(subs)

	def __ne__(self,subs):
		return self.name != str(subs)

	def __ge__(self,subs):
		return self.name >= str(subs)

	def __gt__(self,subs):
		return self.name > str(subs)

def main():
	a = strObj("aazxaacvaa sd")
	b = strObj("AAstrew")
	print "this is index " + a[-5]
	print type(a)
	print type(b)
	print a >> b
	print a+b
	# print a-b
	print ~a
	print a<b
	print a<=b
	print a==b
	print a!=b
	print a>=b
	print a>b
	print a.split("t")
	print a.count("t")
	print a.sort()
	print a.count("aa")
	
if __name__ == "__main__":
	main()
